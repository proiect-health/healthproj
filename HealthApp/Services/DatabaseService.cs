﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HealthApp.Containers;
using HealthApp.Models;
using System.Data;
using Microsoft.Extensions.Configuration;
using System.Reflection;
using System.Data.SqlClient;
using System.ComponentModel;

namespace HealthApp.Services
{
    public class DatabaseService
    {
        public readonly IConfiguration _configuration;
        public DatabaseService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Import()
        {
         
            //UserContainer._users = GetTableInfo<User>("User");
           
            UserContainer._admins = GetTableInfo<Admin>("Admin");
           
            UserContainer._vips = GetTableInfo<Vip>("VipUser");
           
            UserContainer._sports = GetTableInfo<Sport>("SPORT");
           
            UserContainer._liquids = GetTableInfo<Liquid>("LIQUID");
           
            UserContainer._foods = GetTableInfo<Food>("FOOD");
           
            UserContainer._sportStats = GetTableInfo<SportStats>("SPORT_Stats");
           
            UserContainer._liquidStats = GetTableInfo<LiquidStats>("LIQUID_Stats");
          
            UserContainer._foodStats = GetTableInfo<FoodStats>("FOOD_Stats");
           
            UserContainer._sleepStats = GetTableInfo<SleepStats>("SLEEP_Stats");
           
            UserContainer._Friends = GetTableInfo<Friend>("Friends");
            
          
           

        }




        private static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName],null);
                    else
                        continue;
                }
            }
            return obj;
        }

        public List<T> GetTableInfo<T>(string x)
        {
            try{
            string query = @"
                            select * from [dbo].["+ x +"]; " ;
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("HealthProjectDB");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;
                    myCon.Close();
                    myReader.Close();
                }
            }
            return ConvertDataTable<T>(table);
            
            }catch (System.Exception error)
            {
                Console.WriteLine(error.ToString());
                return null;
            }
        }

        public List<T> PostTableAdmin<T>(string username_admin, string password_admin, string email_admin)
        {
            try{
            string query = @"
                            INSERT INTO [dbo].[Admin]
           ([username_admin]
           ,[password_admin]
           ,[email_admin])
     VALUES
           ('"+username_admin+"', '"+password_admin+"', '"+email_admin+"')";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("HealthProjectDB");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;
                    myCon.Close();
                    myReader.Close();
                }
            }
            return ConvertDataTable<T>(table);

            }catch (System.Exception error)
            {
                Console.WriteLine(error.ToString());
                return null;
            }
        }
      public List<T> PostTableVip<T>(string username_vip, string password_vip, string email_vip, float score)
       {
           try
           {
               string query = @"
                           INSERT INTO [dbo].[Vip]
          ([username_Vip]
          ,[password_Vip]
          ,[email_Vip]
          ,[score])
    VALUES
          ('" + username_vip + "', '" + password_vip + "', '" + email_vip+ "', '" + score + "')";
               DataTable table = new DataTable();
               string sqlDataSource = _configuration.GetConnectionString("HealthProjectDB");
               SqlDataReader myReader;
               using (SqlConnection myCon = new SqlConnection(sqlDataSource))
               {
                   myCon.Open();
                   using (SqlCommand myCommand = new SqlCommand(query, myCon))
                   {
                       myReader = myCommand.ExecuteReader();
                       table.Load(myReader); ;
                       myCon.Close();
                       myReader.Close();
                   }
               }
               return ConvertDataTable<T>(table);

            }
            catch (System.Exception error)
            {
                Console.WriteLine(error.ToString());
                return null;
            }
        }
        public List<T> PostTableFood<T>(string name, float calories, float weight, float score_per_gram)
        {
            try
            {
                string query = @"
                           INSERT INTO [dbo].[FOOD]
          ([name]
          ,[calories]
          ,[weight]
          ,[score_per_gram])
    VALUES
          ('" + name + "', '" + calories + "', '" + weight + "','"+score_per_gram+"')";
                DataTable table = new DataTable();
                string sqlDataSource = _configuration.GetConnectionString("HealthProjectDB");
                SqlDataReader myReader;
                using (SqlConnection myCon = new SqlConnection(sqlDataSource))
                {
                    myCon.Open();
                    using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    {
                        myReader = myCommand.ExecuteReader();
                        table.Load(myReader); ;
                        myCon.Close();
                        myReader.Close();
                    }
                }
                return ConvertDataTable<T>(table);

            }
            catch (System.Exception error)
            {
                Console.WriteLine(error.ToString());
                return null;
            }
        }

        public List<T> PostTableLiquid<T>(string name, float calories, float weight, float score_per_ml)
        {
            try
            {
                string query = @"
                           INSERT INTO [dbo].[LIQUID]
          ([name]
          ,[calories]
          ,[weight]
          ,[score_per_ml])
    VALUES
          ('" + name + "', '" + calories + "', '" + weight + "','" + score_per_ml + "')";
                DataTable table = new DataTable();
                string sqlDataSource = _configuration.GetConnectionString("HealthProjectDB");
                SqlDataReader myReader;
                using (SqlConnection myCon = new SqlConnection(sqlDataSource))
                {
                    myCon.Open();
                    using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    {
                        myReader = myCommand.ExecuteReader();
                        table.Load(myReader); ;
                        myCon.Close();
                        myReader.Close();
                    }
                }
                return ConvertDataTable<T>(table);

            }
            catch (System.Exception error)
            {
                Console.WriteLine(error.ToString());
                return null;
            }
        }
        public List<T> PostTableSport<T>(string name, float score_per_min)
        {
            try
            {
                string query = @"
                           INSERT INTO [dbo].[SPORT]
          ([name]
          ,[score_per_min])
    VALUES
          ('" + name + "', '" + score_per_min + "')";
                DataTable table = new DataTable();
                string sqlDataSource = _configuration.GetConnectionString("HealthProjectDB");
                SqlDataReader myReader;
                using (SqlConnection myCon = new SqlConnection(sqlDataSource))
                {
                    myCon.Open();
                    using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    {
                        myReader = myCommand.ExecuteReader();
                        table.Load(myReader); ;
                        myCon.Close();
                        myReader.Close();
                    }
                }
                return ConvertDataTable<T>(table);

            }
            catch (System.Exception error)
            {
                Console.WriteLine(error.ToString());
                return null;
            }
        }

        public List<T> PostTableFoodStats<T>(float calories, int id_food, int id_user)
        {
            try
            {
                string query = @"
                           INSERT INTO [dbo].[FOOD_Stats]
          ([calories]
          ,[id_food]
          ,[id_user])
    VALUES
          ('" + calories + "', '" + id_food + "', '" + id_user + "')";
                DataTable table = new DataTable();
                string sqlDataSource = _configuration.GetConnectionString("HealthProjectDB");
                SqlDataReader myReader;
                using (SqlConnection myCon = new SqlConnection(sqlDataSource))
                {
                    myCon.Open();
                    using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    {
                        myReader = myCommand.ExecuteReader();
                        table.Load(myReader); ;
                        myCon.Close();
                        myReader.Close();
                    }
                }
                return ConvertDataTable<T>(table);

            }
            catch (System.Exception error)
            {
                Console.WriteLine(error.ToString());
                return null;
            }
        }
        public List<T> PostTableLiquidStats<T>(float quantity, int id_liquid, int id_user)
        {
            try
            {
                string query = @"
                           INSERT INTO [dbo].[LIQUID_Stats]
          ([quantity]
          ,[id_liquid]
          ,[id_user])
    VALUES
          ('" + quantity + "', '" + id_liquid + "', '" + id_user + "')";
                DataTable table = new DataTable();
                string sqlDataSource = _configuration.GetConnectionString("HealthProjectDB");
                SqlDataReader myReader;
                using (SqlConnection myCon = new SqlConnection(sqlDataSource))
                {
                    myCon.Open();
                    using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    {
                        myReader = myCommand.ExecuteReader();
                        table.Load(myReader); ;
                        myCon.Close();
                        myReader.Close();
                    }
                }
                return ConvertDataTable<T>(table);

            }
            catch (System.Exception error)
            {
                Console.WriteLine(error.ToString());
                return null;
            }
        }

        public List<T> PostTableSportStats<T>(float distance, float calories, string time, int id_sport, int id_user)
        {
            try
            {
                string query = @"
                           INSERT INTO [dbo].[SPORT_Stats]
          ([distance]
          ,[calories]
          ,[time]
          ,[id_sport]
          ,[id_user])
    VALUES
          ('" + distance + "', '" + calories + "', '" + time + "', '"+ id_sport + "', '"+ id_user + "')";
                DataTable table = new DataTable();
                string sqlDataSource = _configuration.GetConnectionString("HealthProjectDB");
                SqlDataReader myReader;
                using (SqlConnection myCon = new SqlConnection(sqlDataSource))
                {
                    myCon.Open();
                    using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    {
                        myReader = myCommand.ExecuteReader();
                        table.Load(myReader); ;
                        myCon.Close();
                        myReader.Close();
                    }
                }
                return ConvertDataTable<T>(table);

            }
            catch (System.Exception error)
            {
                Console.WriteLine(error.ToString());
                return null;
            }
        }
        public List<T> PostTableSleepStats<T>(string sleep_time, float sleep_score, int id_user)
        {
            try
            {
                string query = @"
                           INSERT INTO [dbo].[SLEEP_Stats]
          ([sleep_time]
          ,[sleep_score]
          ,[id_user])
    VALUES
          ('" + sleep_time + "', '" + sleep_score + "', '" + id_user + "')";
                DataTable table = new DataTable();
                string sqlDataSource = _configuration.GetConnectionString("HealthProjectDB");
                SqlDataReader myReader;
                using (SqlConnection myCon = new SqlConnection(sqlDataSource))
                {
                    myCon.Open();
                    using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    {
                        myReader = myCommand.ExecuteReader();
                        table.Load(myReader); ;
                        myCon.Close();
                        myReader.Close();
                    }
                }
                return ConvertDataTable<T>(table);

            }
            catch (System.Exception error)
            {
                Console.WriteLine(error.ToString());
                return null;
            }
        }

        public List<T> PostTableFriends<T>(int id_user, int id_user_friend)
        {
            try
            {
                string query = @"
                           INSERT INTO [dbo].[Friends]
          ([id_user]
          ,[id_user_friend])
    VALUES
          ('" + id_user + "', '" + id_user_friend + "')";
                DataTable table = new DataTable();
                string sqlDataSource = _configuration.GetConnectionString("HealthProjectDB");
                SqlDataReader myReader;
                using (SqlConnection myCon = new SqlConnection(sqlDataSource))
                {
                    myCon.Open();
                    using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    {
                        myReader = myCommand.ExecuteReader();
                        table.Load(myReader); ;
                        myCon.Close();
                        myReader.Close();
                    }
                }
                return ConvertDataTable<T>(table);

            }
            catch (System.Exception error)
            {
                Console.WriteLine(error.ToString());
                return null;
            }
        }
    }

    
}
