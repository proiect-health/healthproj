﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HealthApp.Containers;
using HealthApp.Models;
using System.Collections.Generic;
using System.Linq;

namespace HealthApp.Services
{
    public class RankingService
    {
        
        
        public static List<User> SorterRanking()
        {
            List<User> rankedusers = new List<User>();
            foreach(User user in UserContainer._vips)
            {
                rankedusers.Add(user);
            }
            foreach (User user in UserContainer._users)
            {
                rankedusers.Add(user);
            }
            rankedusers=rankedusers.OrderByDescending(o=>o.score).ToList();
            
            return rankedusers;
        }
        public static List<User> SorterRanking(List<User> users)
        {
            List<User> rankedusers = new List<User>();
            foreach (User user in UserContainer._users)
            {
                rankedusers.Add(user);
            }
            
            rankedusers = rankedusers.OrderByDescending(o => o.score).ToList();

            return rankedusers;
        }

    }
}
