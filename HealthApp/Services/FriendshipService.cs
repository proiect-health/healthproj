﻿using System;
using HealthApp.Models;
using HealthApp.Containers;

namespace HealthApp.Services
{
    public class FriendshipService
    {
       public static void EstablishFriendship(int id1,int id2)
        {
            Friend friend = new Friend();
            friend.id_user = id1;
            friend.id_user_friend= id2;
            UserContainer._Friends.Add(friend);
        }

    }
}
