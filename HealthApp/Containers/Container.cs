﻿using System.Collections.Generic;
using HealthApp.Models;

namespace HealthApp.Containers
{
    public class UserContainer
    {
        public static List<User> _users = new List<User>();
        public static List<Admin> _admins = new List<Admin>();
        public static List<Vip> _vips = new List<Vip>();
        public static List<Sport> _sports = new List<Sport>();
        public static List<SportStats> _sportStats = new List<SportStats>();
        public static List<Friend> _Friends = new List<Friend>();
        public static List<Liquid> _liquids = new List<Liquid>();
        public static List<LiquidStats> _liquidStats = new List<LiquidStats>();
        public static List<Food> _foods = new List<Food>();
        public static List<FoodStats> _foodStats = new List<FoodStats>();
        public static List<SleepStats> _sleepStats = new List<SleepStats>();
     

    }
}
