﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthApp.Models
{
    public class Sport
    {
        public int id_sport { get; set; }
        public string name { get; set; }
        public float score_per_min { get; set; }

    }
}
