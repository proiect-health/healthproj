﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthApp.Models
{
    public class SleepStats
    {
        public int id_stats_sleep { get; set; }
        public string sleep_time { get; set; }
        public float sleep_score { get; set; }
        public int id_user { get; set; }

    }
}
