﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthApp.Models
{
    public class FoodStats
    {
        public int id_food_stats { get; set; }
        public float calories { get; set; }
        public int id_food { get; set; }
        public int id_user { get; set; }

    }
}
