﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthApp.Models
{
    public class SportStats
    {
        public int id_sport_stats { get; set; }
        public float distance { get; set; }
        public float calories { get; set; }
        public string time { get; set; }
        public int id_sport { get; set; }
        public int id_user { get; set; }

    }
}
