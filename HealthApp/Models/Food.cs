﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthApp.Models
{
    public class Food
    {
        public int id_food { get; set; }
        public string name { get; set; }
        public float calories { get; set; }
        public float weight { get; set; }
        public float score_per_gram { get; set; }
    }
}
