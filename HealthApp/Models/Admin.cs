﻿using System;
namespace HealthApp.Models
{
    public class Admin
    {
       public int id_admin { get; set; }
        public string email_admin { get; set; }
        public string username_admin { get; set; }
        public string password_admin { get; set; }
        
    }
}
