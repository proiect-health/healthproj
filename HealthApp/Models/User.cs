﻿using System;
namespace HealthApp.Models
{
    public class User
    {
       public int Id_User { get; set; }
        public string email { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public float score { get; set; }
        
    }
}
