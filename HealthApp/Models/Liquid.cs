﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthApp.Models
{
    public class Liquid
    {
        public int id_liquid { get; set; }
        public string name { get; set; }

        public float calories { get; set; }
        public float weight { get; set; }
        public float score_per_ml { get; set; }

    }
}
