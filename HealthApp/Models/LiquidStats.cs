﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthApp.Models
{
    public class LiquidStats
    {
        public int id_liquid_stats { get; set; }
        public float quantity { get; set; }
        public int id_liquid { get; set; }
        public int id_user { get; set; }

    }
}
