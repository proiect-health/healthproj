﻿using System;
namespace HealthApp.Models
{
    public class Friend
    {
        public int id_friends_table { set; get; }
        public int id_user { set; get; }
        public int id_user_friend { set; get; }
    }
}
