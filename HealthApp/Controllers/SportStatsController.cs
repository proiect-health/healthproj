﻿using HealthApp.Containers;
using HealthApp.Models;
using HealthApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace HealthApp.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class SportStatsController : ControllerBase
    {
        public readonly IConfiguration _configuration;
        public SportStatsController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpGet]
        public List<SportStats> Get()
        {

            DatabaseService db=new DatabaseService(_configuration);
            db.Import();
            return UserContainer._sportStats;
        }
        [HttpPost]
        public async Task<ObjectResult> Post(SportStats sportStats)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");
           
          
           if(sportStats.id_user == 0) { return BadRequest("null user id."); }
           if(sportStats.time == null) { return BadRequest("null time"); }
           if(sportStats.calories == 0) { return BadRequest("null calories"); }
           if(sportStats.distance == 0) { return BadRequest("null distance"); }

            DatabaseService db=new DatabaseService(_configuration);
            db.PostTableSportStats<SportStats>(sportStats.distance, sportStats.calories, sportStats.time,sportStats.id_sport,sportStats.id_user);
            db.Import();
            return Ok("Done!");
        }
        [HttpPut]
        public async Task<ObjectResult> Put(SportStats sportStats)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");

        
            var specificSportStats = UserContainer._sportStats.Where(s => s.id_sport_stats == sportStats.id_sport_stats).FirstOrDefault<SportStats>();
            if (specificSportStats != null)
            {
                specificSportStats.id_sport_stats = sportStats.id_sport_stats;
                specificSportStats.id_sport = sportStats.id_sport;
                specificSportStats.id_user = sportStats.id_user;
                specificSportStats.time = sportStats.time;
                specificSportStats.distance = sportStats.distance;
                specificSportStats.calories = sportStats.calories;

            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }

        [HttpDelete("{id}")]
        public async Task<ObjectResult> Delete(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");

            var specificSport = UserContainer._sportStats.Where(s => s.id_sport == id).FirstOrDefault<SportStats>();
            if (specificSport != null)
            {
                UserContainer._sportStats.Remove(specificSport);

            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }

    }
}
