﻿using HealthApp.Containers;
using HealthApp.Models;
using HealthApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HealthApp.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class FriendsController : ControllerBase
    {
        public readonly IConfiguration _configuration;
        public FriendsController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet("{username}")]
        public async Task<ObjectResult> Get(string username)
        {
            DatabaseService db=new DatabaseService(_configuration);
          
            db.Import();
            string fusername = null;
            int id = 0;
            id =UserContainer._users.Find(s => s.username == username).Id_User;
            if (id == 0) {
                id = UserContainer._vips.Find(s => s.username == username).Id_User;
                if (id == 0)
                {
                    return NotFound("Not Found");
                }
            }
            
                foreach (Friend Friendship in UserContainer._Friends)
            {
                if (Friendship.id_user == id)
                {
                    string findresult = UserContainer._users.Find(s => s.Id_User == Friendship.id_user_friend).username;
                    if(findresult==null)
                    {
                        findresult = UserContainer._vips.Find(s => s.Id_User == Friendship.id_user_friend).username;
                        
                        
                    }
                    if (findresult != null)
                        fusername +=findresult+",";
                }
            }
            if (fusername == null)
            {
                return NotFound("Not Found");
            }
            return Ok($"{username}:{fusername}");
        }
        [HttpPost("FriendshipAdd")]
        public async Task<ObjectResult> Post(string username1,string username2)
        {
            int id1 = 0;
            id1 =UserContainer._users.Find(s => s.username == username1).Id_User;
            if(id1==0)
            {
                id1 = UserContainer._vips.Find(s => s.username == username1).Id_User;
                if (id1 == 0)
                {
                    return NotFound("Not Found");
                }
            }
            int id2 = 0;
            id2 = UserContainer._users.Find(s => s.username == username2).Id_User;
            if (id2 == 0)
            {
                id1 = UserContainer._vips.Find(s => s.username == username2).Id_User;
                if (id2 == 0)
                {
                    return NotFound("Not Found");
                }
            }
            Friend fr = new Friend();
            FriendshipService.EstablishFriendship(id1, id2);
            FriendshipService.EstablishFriendship(id2, id1);
            DatabaseService db=new DatabaseService(_configuration);
            db.PostTableFriends<Friend>(id1,id2);
            db.Import();
            return Ok("Friendship Added!");
        }
        [HttpDelete]
        public async Task<ObjectResult> Delete(string username1,string username2)
        {
            int id1 = 0;
            id1 = UserContainer._users.Find(s => s.username == username1).Id_User;
            if (id1 == 0)
            {
                id1 = UserContainer._vips.Find(s => s.username == username1).Id_User;
                if (id1 == 0)
                {
                    return NotFound("Not Found");
                }
            }
            int id2 = 0;
            id1 = UserContainer._users.Find(s => s.username == username2).Id_User;
            if (id2 == 0)
            {
                id2 = UserContainer._vips.Find(s => s.username == username2).Id_User;
                if (id2 == 0)
                {
                    return NotFound("Not Found");
                }
            }
            var Friendship = UserContainer._Friends.Find(s => s.id_user == id1 && s.id_user_friend== id2);
            if (Friendship == null)
            {
                return NotFound("That Friendship Doesn't Exist!");
            }
            UserContainer._Friends.Remove(Friendship);
            return Ok("Done!");
        }

    }
}
