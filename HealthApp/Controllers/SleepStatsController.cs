﻿using HealthApp.Containers;
using HealthApp.Models;
using HealthApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthApp.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class SleepStatsController : ControllerBase
    {
        public readonly IConfiguration _configuration;
        public SleepStatsController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpGet]
        public List<SleepStats> Get()
        {

            DatabaseService db=new DatabaseService(_configuration);
            db.Import();
            return UserContainer._sleepStats;
        }
        [HttpPost]
        public async Task<ObjectResult> Post(SleepStats sleepStats)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            if (sleepStats.id_stats_sleep == 0) { return BadRequest("null sleep stats id."); }
            if (sleepStats.id_user == 0) { return BadRequest("null user id."); }
            if (sleepStats.sleep_score == 0) { return BadRequest("null sleep score."); }
            if (sleepStats.sleep_time == null) { return BadRequest("null sleep time."); }
            

            DatabaseService db=new DatabaseService(_configuration);
            db.PostTableSleepStats<SleepStats>(sleepStats.sleep_time, sleepStats.sleep_score,sleepStats.id_user);
            db.Import();
            return Ok("Done!");
        }
        [HttpPut]
        public async Task<ObjectResult> Put(SleepStats sleepStats)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");

            var specificSleepStats = UserContainer._sleepStats.Where(s => s.id_stats_sleep == sleepStats.id_stats_sleep).FirstOrDefault<SleepStats>();
            if (specificSleepStats != null)
            {
                specificSleepStats.id_stats_sleep = sleepStats.id_stats_sleep;
                specificSleepStats.id_user = sleepStats.id_user;
                specificSleepStats.sleep_score = sleepStats.sleep_score;
                specificSleepStats.sleep_time = sleepStats.sleep_time;
            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }
        [HttpDelete("{id}")]
        public async Task<ObjectResult> Delete(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");

            var specificSleepStats = UserContainer._sleepStats.Where(s => s.id_stats_sleep == id).FirstOrDefault<SleepStats>();
            if (specificSleepStats != null)
            {
                UserContainer._sleepStats.Remove(specificSleepStats);

            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }
    }
}
