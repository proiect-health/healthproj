﻿using HealthApp.Containers;
using HealthApp.Models;
using HealthApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthApp.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class FoodStatsController : ControllerBase
    {
        public readonly IConfiguration _configuration;
        public FoodStatsController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpGet]
        public List<FoodStats> Get()
        {
            DatabaseService db=new DatabaseService(_configuration);
            db.Import();
            return UserContainer._foodStats;
        }
        [HttpPost]
        public async Task<ObjectResult> Post(FoodStats foodStats)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            if(foodStats.id_food_stats == 0) { return BadRequest("null food stats id."); }
            if(foodStats.id_food == 0) { return BadRequest("null food id."); }
            if(foodStats.id_user == 0) { return BadRequest("null user id."); }
            if(foodStats.calories == 0) { return BadRequest("null calories"); }

            DatabaseService db=new DatabaseService(_configuration);
            db.PostTableFoodStats<FoodStats>(foodStats.calories, foodStats.id_food, foodStats.id_user);
            db.Import();
            return Ok("Done!");
        }
        [HttpPut]
        public async Task<ObjectResult> Put(FoodStats foodStats)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");

            var specificFoodStats = UserContainer._foodStats.Where(s => s.id_food_stats == foodStats.id_food_stats).FirstOrDefault<FoodStats>();
            if (specificFoodStats != null)
            {
                specificFoodStats.id_food_stats = foodStats.id_food_stats;
                specificFoodStats.id_food = foodStats.id_food;
                specificFoodStats.id_user = foodStats.id_user;
                specificFoodStats.calories = foodStats.calories;
            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }
        [HttpDelete("{id}")]
        public async Task<ObjectResult> Delete(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");

            var specificFoodStats = UserContainer._foodStats.Where(s => s.id_food_stats == id).FirstOrDefault<FoodStats>();
            if (specificFoodStats != null)
            {
                UserContainer._foodStats.Remove(specificFoodStats);

            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }
    }
}
