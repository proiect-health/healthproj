﻿using HealthApp.Containers;
using HealthApp.Models;
using HealthApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthApp.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class SportController : ControllerBase
    {
        public readonly IConfiguration _configuration;
        public SportController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpGet]
        public List<Sport> Get()
        {

            DatabaseService db=new DatabaseService(_configuration);
            db.Import();
            return UserContainer._sports;
        }
        [HttpPost("{adminname}/add")]
        public async Task<ObjectResult> Post(string adminname, Sport sport)
        {
            var admin = UserContainer._admins.Find(s => s.username_admin == adminname);
            if (admin == null)
            {
                return BadRequest("The Admin Doesn't Exist");
            }

            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");
           
            if (sport.name == null) { return BadRequest("null name."); }
            if (sport.score_per_min == 0) { return BadRequest("null score per minute."); }
            if(sport.id_sport == 0) { return BadRequest("null sport id."); }
            DatabaseService db=new DatabaseService(_configuration);
            db.PostTableSport<Sport>(sport.name,sport.score_per_min);
            db.Import();
            return Ok("Done!");
        }
        [HttpPut]
        public async Task<ObjectResult> Put(Sport sport)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");
      
            var specificSport = UserContainer._sports.Where(s => s.id_sport == sport.id_sport).FirstOrDefault<Sport>();
            if (specificSport != null)
            {
                specificSport.id_sport = sport.id_sport;
                specificSport.name = sport.name;
                specificSport.score_per_min = sport.score_per_min;

            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }
        [HttpDelete(("{adminname}/delete/{id}"))]
        public async Task<ObjectResult> Delete(string adminname, int id)
        {
            var admin = UserContainer._admins.Find(s => s.username_admin == adminname);
            if (admin == null)
            {
                return BadRequest("The Admin Doesn't Exist");
            }
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");
  
            var specificSport = UserContainer._sports.Where(s => s.id_sport == id).FirstOrDefault<Sport>();
            if (specificSport != null)
            {
                UserContainer._sports.Remove(specificSport);

            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }
        
    }
}
