﻿using HealthApp.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HealthApp.Containers;
using Microsoft.Extensions.Configuration;
using HealthApp.Services;

namespace HealthApp.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class VipController : ControllerBase
    {
        public readonly IConfiguration _configuration;
        public VipController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public List<Vip> Get()
        {

            DatabaseService db=new DatabaseService(_configuration);
            db.Import();
            return UserContainer._vips;
        }
        [HttpPost]
        public async Task<ObjectResult> Post(Vip vip)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");
            if (vip.username == null) { return BadRequest("null username."); }
            if (vip.password == null) { return BadRequest("null password."); }
            DatabaseService db=new DatabaseService(_configuration);
            db.PostTableVip<Vip>(vip.username,vip.password,vip.email,vip.score);
            db.Import();
            return Ok("Done!");
        }
        [HttpPut]
        public async Task<ObjectResult> Put(Vip vip){
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");
            var specificUser = UserContainer._vips.Where(s => s.id_vip == vip.id_vip).FirstOrDefault<Vip>();
            if (specificUser != null)
            {
                specificUser.username = vip.username;
                specificUser.password = vip.password;


            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }
        [HttpDelete("{id}")]
        public async Task<ObjectResult> Delete(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");
            var specificUser = UserContainer._vips.Where(s => s.id_vip == id).FirstOrDefault<Vip>();
            if (specificUser != null)
            {
                UserContainer._vips.Remove(specificUser);

            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }

    }
}
