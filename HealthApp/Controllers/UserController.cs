﻿using HealthApp.Containers;
using HealthApp.Models;
using HealthApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HealthApp.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class UserController : ControllerBase
    {

        public readonly IConfiguration _configuration;
        public UserController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public List<User> Get()
        {
            DatabaseService db=new DatabaseService(_configuration);
            db.Import();
            return UserContainer._users;
        }
        [HttpPost]
        public async Task<ObjectResult> Post(User user)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");
            if (user.username == null) { return BadRequest("null username."); }
            if (user.password == null) { return BadRequest("null password."); }
            User usr = new User();
            usr.Id_User = user.Id_User;
            usr.username = user.username;
            usr.password = user.password;
            usr.email = user.email;
            usr.score = user.score;
            UserContainer._users.Add(usr);
            return Ok("Done!");
        }
        [HttpPut]
        public async Task<ObjectResult> Put(User user)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");
            var specificUser = UserContainer._users.Where(s => s.Id_User == user.Id_User).FirstOrDefault<User>();
            if (specificUser != null)
            {
                specificUser.username = user.username;
                specificUser.password = user.password;


            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }
        [HttpDelete("{id}")]
        public async Task<ObjectResult> Delete(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");
            var specificUser = UserContainer._users.Where(s => s.Id_User == id).FirstOrDefault<User>();
            if (specificUser != null)
            {
                UserContainer._users.Remove(specificUser);

            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }


    }
}
