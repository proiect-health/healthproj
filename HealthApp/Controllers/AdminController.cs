﻿using HealthApp.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HealthApp.Containers;
using Microsoft.Extensions.Configuration;
using HealthApp.Services;

namespace HealthApp.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class AdminController : ControllerBase
    {
        public readonly IConfiguration _configuration;
        public AdminController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public List<Admin> Get()
        {
            DatabaseService db=new DatabaseService(_configuration);
            db.Import();
            return UserContainer._admins;
        }
        [HttpPost]
        public async Task<ObjectResult> Post(Admin admin)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");
            if (admin.username_admin == null) { return BadRequest("null username."); }
            if (admin.password_admin == null) { return BadRequest("null password."); }   
            DatabaseService db=new DatabaseService(_configuration);
            db.PostTableAdmin<Admin>(admin.username_admin,admin.password_admin,admin.email_admin);
            db.Import();
            return Ok("Done!");
        }
        [HttpPut]
        public async Task<ObjectResult> Put(Admin admin)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");
            var specificUser = UserContainer._admins.Where(s => s.id_admin == admin.id_admin).FirstOrDefault<Admin>();
            if (specificUser != null)
            {
                specificUser.username_admin = admin.username_admin;
                specificUser.password_admin = admin.password_admin;
                specificUser.email_admin = admin.email_admin;

            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }
        [HttpDelete("{id}")]
        public async Task<ObjectResult> Delete(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");
            var specificUser = UserContainer._admins.Where(s => s.id_admin == id).FirstOrDefault<Admin>();
            if (specificUser != null)
            {
                UserContainer._admins.Remove(specificUser);

            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }
        
    }
}
