﻿using HealthApp.Containers;
using HealthApp.Models;
using HealthApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthApp.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class LiquidStatsController : ControllerBase
    {
        public readonly IConfiguration _configuration;
        public LiquidStatsController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpGet]
        public List<LiquidStats> Get()
        {
            DatabaseService db=new DatabaseService(_configuration);
            db.Import();

            return UserContainer._liquidStats;
        }
        [HttpPost]
        public async Task<ObjectResult> Post(LiquidStats liquidStats)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            if (liquidStats.id_liquid_stats == 0) { return BadRequest("null liquid stats id."); }
            if (liquidStats.id_liquid == 0) { return BadRequest("null liquid id."); }
            if (liquidStats.id_user == 0) { return BadRequest("null user id."); }
            if(liquidStats.quantity == 0) { return BadRequest("null quantity."); }

            DatabaseService db=new DatabaseService(_configuration);
            db.PostTableLiquidStats<LiquidStats>(liquidStats.quantity, liquidStats.id_liquid, liquidStats.id_user);
            db.Import();
            return Ok("Done!");
        }
        [HttpPut]
        public async Task<ObjectResult> Put(LiquidStats liquidStats)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");

            var specificLiquidStats = UserContainer._liquidStats.Where(s => s.id_liquid_stats == liquidStats.id_liquid_stats).FirstOrDefault<LiquidStats>();
            if (specificLiquidStats != null)
            {
                specificLiquidStats.id_liquid_stats = liquidStats.id_liquid_stats;
                specificLiquidStats.id_liquid = liquidStats.id_liquid;
                specificLiquidStats.id_user = liquidStats.id_user;
                specificLiquidStats.quantity = liquidStats.quantity;
            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }
        [HttpDelete("{id}")]
        public async Task<ObjectResult> Delete(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");

            var specificLiquidStats = UserContainer._liquidStats.Where(s => s.id_liquid_stats == id).FirstOrDefault<LiquidStats>();
            if (specificLiquidStats != null)
            {
                UserContainer._liquidStats.Remove(specificLiquidStats);

            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }

    }
}
