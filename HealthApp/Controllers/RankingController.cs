﻿using HealthApp.Containers;
using HealthApp.Models;
using HealthApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HealthApp.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class RankingController : ControllerBase
    {
        public readonly IConfiguration _configuration;
        public RankingController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpGet]
        public string Get()
        {
            string result=new string("---USER RANKING---\n");
            List<User> topUsers= RankingService.SorterRanking();
            
            foreach (User user in topUsers)
            {
                result += user.username+"->"+user.score.ToString()+"\n";
            }
            return result;

            
        }
        [HttpGet("Friendship")]
        public async Task<ObjectResult> GetFr(string username)
        {
            List<User> friends=new List<User>();
            string result = new string("---Friends RANKING---\n");
            var user_id = 0;
            user_id=UserContainer._users.Find(s => s.username == username).Id_User;
            
            if (user_id == 0)
            {
                user_id = UserContainer._vips.Find(s => s.username == username).Id_User;
                if (user_id == 0)
                {
                    return NotFound("Not Found");
                }
            }
            foreach (Friend Friendship in UserContainer._Friends)
            {
                
                if (Friendship.id_user == user_id)
                {
                    var friend = UserContainer._users.Find(s => s.Id_User == Friendship.id_user_friend);
                    if (friend == null)
                    {
                        friend = UserContainer._vips.Find(s => s.Id_User == Friendship.id_user_friend);
                        if (friend == null)
                        {
                            return NotFound("Not Found");
                        }
                    }
                    friends.Add(friend);
                }
                   
            }
            if(friends.Capacity==0)
            {
                return NotFound("Not Found");
            }
            List<User> topUsers = RankingService.SorterRanking(friends);

            foreach (User user in topUsers)
            {
                result += user.username + "->" + user.score.ToString() + "\n";
            }
            return Ok(result);


        }
    }
}
