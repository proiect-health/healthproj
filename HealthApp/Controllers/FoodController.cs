﻿using HealthApp.Containers;
using HealthApp.Models;
using HealthApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthApp.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class FoodController : ControllerBase
    {
        public readonly IConfiguration _configuration;
        public FoodController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpGet]
        public List<Food> Get()
        {

            DatabaseService db=new DatabaseService(_configuration);
            db.Import();
            return UserContainer._foods;
        }
        [HttpPost("{adminname}/add")]
        public async Task<ObjectResult> Post(string adminname, Food food)
        {
            var admin = UserContainer._admins.Find(s => s.username_admin == adminname);
            if (admin == null)
            {
                return BadRequest("The Admin Doesn't Exist");
            }
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            if (food.id_food == 0) { return BadRequest("null food id."); }
            if (food.name == null) { return BadRequest("null food name."); }
            if (food.score_per_gram == 0) { return BadRequest("null score per gram."); }
            if (food.weight == 0) { return BadRequest("null weight."); }
            if (food.calories == 0) { return BadRequest("null calories."); }

            
             DatabaseService db=new DatabaseService(_configuration);
            db.PostTableFood<Food>(food.name, food.calories, food.weight, food.score_per_gram);
            db.Import();
            return Ok("Done!");
        }
        [HttpPut]
        public async Task<ObjectResult> Put(Food food)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");

            var specificFood = UserContainer._foods.Where(s => s.id_food == food.id_food).FirstOrDefault<Food>();
            if (specificFood != null)
            {
                specificFood.id_food = food.id_food;
                specificFood.name = food.name;
                specificFood.weight = food.weight;
                specificFood.score_per_gram = food.score_per_gram;
                specificFood.calories = food.calories;
            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }
        [HttpDelete("{adminname}/delete/{id}")]
        public async Task<ObjectResult> Delete(string adminname, int id)
        {
            var admin = UserContainer._admins.Find(s => s.username_admin == adminname);
            if (admin == null)
            {
                return BadRequest("The Admin Doesn't Exist");
            }
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");

            var specificFood = UserContainer._foods.Where(s => s.id_food == id).FirstOrDefault<Food>();
            if (specificFood != null)
            {
                UserContainer._foods.Remove(specificFood);

            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }
    }
}
