﻿using HealthApp.Containers;
using HealthApp.Models;
using HealthApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace HealthApp.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class LiquidController : ControllerBase
    {
        public readonly IConfiguration _configuration;
        public LiquidController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpGet]
        public List<Liquid> Get()
        {

            DatabaseService db=new DatabaseService(_configuration);
            db.Import();
            return UserContainer._liquids;
        }
        [HttpPost("{adminname}/add")]
        public async Task<ObjectResult> Post(string adminname, Liquid liquid)
        {
            var admin = UserContainer._admins.Find(s => s.username_admin == adminname);
            if (admin == null)
            {
                return BadRequest("The Admin Doesn't Exist");
            }
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            if (liquid.id_liquid == 0) { return BadRequest("null liquid id."); }
            if (liquid.name == null) { return BadRequest("null liquid name."); }
            if (liquid.score_per_ml == 0) { return BadRequest("null liquid score per ml."); }
            if (liquid.weight == 0) { return BadRequest("null weight."); }
            if (liquid.calories == 0) { return BadRequest("null calories."); }

            DatabaseService db=new DatabaseService(_configuration);
            db.PostTableLiquid<Liquid>(liquid.name,liquid.calories,liquid.weight,liquid.score_per_ml);
            db.Import();
            return Ok("Done!");
        }
        [HttpPut]
        public async Task<ObjectResult> Put(Liquid liquid)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");

            var specificLiquid = UserContainer._liquids.Where(s => s.id_liquid == liquid.id_liquid).FirstOrDefault<Liquid>();
            if(specificLiquid != null)
            {
                specificLiquid.id_liquid = liquid.id_liquid;
                specificLiquid.name = liquid.name;
                specificLiquid.weight = liquid.weight;
                specificLiquid.score_per_ml = liquid.score_per_ml;
                specificLiquid.calories = liquid.calories;
            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }
        [HttpDelete("{adminname}/delete/{id}")]
        public async Task<ObjectResult> Delete(string adminname, int id)
        {
            var admin = UserContainer._admins.Find(s => s.username_admin == adminname);
            if (admin == null)
            {
                return BadRequest("The Admin Doesn't Exist");
            }
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");

            var specificLiquid = UserContainer._liquids.Where(s => s.id_liquid == id).FirstOrDefault<Liquid>();
            if (specificLiquid != null)
            {
                UserContainer._liquids.Remove(specificLiquid);

            }
            else
            {
                return NotFound("Not Found");
            }

            return Ok("Done!");
        }

    }
}
